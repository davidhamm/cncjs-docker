# What is CNCjs?

A web-based interface for CNC milling controller running Grbl, Smoothieware, or TinyG. It runs on an Raspberry Pi or a laptop computer that you have Node.js installed, connecting to the Arduino over a serial connection using a USB serial port, a Bluetooth serial module, or a Serial-to-WiFi module like XBee or USR-WIFI232-T.

<img src="https://cloud.githubusercontent.com/assets/447801/24392019/aa2d725e-13c4-11e7-9538-fd5f746a2130.png" alt="CNCjs" width="100%"/>

# How to use this image

## Start a CNCjs instance
```
docker run -d \
           --name cncjs \
           --privileged \
           --device /dev/ttyUSB0:/dev/ttyACM0 \
           -p 80:8000 \
           --restart always \
       davidhamm/cncjs:latest
```

- **`--privileged`**  
  is necessary vor CNCjs to match Container intern user rights to Host user rights  
- **`--device /dev/ttyUSB0:/dev/ttyACM0`**  
  mounts the actual serial port to which your cnc is connected into the container. please check your serial port before mounting  
- **`-p 80:8000`**  
  binds the host port '80' to the coinainers internal port '8000'. CNCjs is listening by default on port '8000' inside the container  
- **`--restart always`**  
  restarts the container always as long as the user doesn't shut's it down

## Webinterface

CNCjs will be available on this URL:

```
http://localhost
```
