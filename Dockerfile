FROM node:10-alpine
LABEL maintainer="David Hamm <david.hamm@hammwerk.de>"

# install python
RUN apk add --update g++ make python linux-headers

RUN npm install npm@latest -g
RUN npm install -g cncjs@v1.9.20 --unsafe-perm

ENTRYPOINT cncjs
